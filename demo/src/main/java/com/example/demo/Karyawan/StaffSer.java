package com.example.demo.Karyawan;
public class StaffSer extends Worker{
    private int tunjMakan = 220000;
    private int tunjTransport = 440000;
    private int totalGaji;
    private int idCari;

    public Integer setIdCari(int idCari) {
        this.idCari = idCari;
        return idCari;
    }

    public int getIdCari() {
        int gaji, cuti, jlAbsensi, tjMakan, tjTransport;
        jlAbsensi = jumlahAbsensi[idCari - 1];
        gaji = (gapok[idCari - 1] / 22) * jlAbsensi;
        cuti = jlhCutiTerpakai[idCari - 1] - 1;

        //Tunjangan Makan dan transport
        if (jlAbsensi > 1) {
            tjMakan = tunjMakan - ((tunjMakan / 22) * cuti);
            tjTransport = tunjTransport - ((tunjTransport / 22) * cuti);
        } else {
            tjMakan = tunjMakan;
            tjTransport = tunjTransport;
        }

        System.out.println("Id\t\t\t\t: " + id[idCari - 1] + "\nNama\t\t\t: " + nama[idCari - 1] + "\nGaji Pokok \t\t: " + gapok[idCari - 1] + "\nJumlah Absensi \t: " + jumlahAbsensi[idCari - 1] + "\nJumlah Cuti \t: " + jlhCutiTerpakai[idCari - 1]);
        System.out.println("Tunjangan Makan : "+tjMakan);
        System.out.println("Tunjangan Transport : "+tjTransport);
        System.out.println("Gaji\t\t\t: "+gaji);

        totalGaji = gaji + tjMakan + tjTransport;
        System.out.println();
        System.out.println("Total Gaji Karyawan dengan id " + idCari + " adalah " + totalGaji);
        return totalGaji;
    }
}


