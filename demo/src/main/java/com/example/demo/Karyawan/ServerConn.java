package com.example.demo.Karyawan;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerConn extends Thread {

    public ServerConn(int port) throws IOException {
        ServerSocket serverSocket = new ServerSocket(port);
//        while(true) {
            try {
                StaffSer staffSer = new StaffSer();
//                staffSer.setIdCari(3);
                System.out.println("Waiting for client on port " + serverSocket.getLocalPort() + "...");
                Socket server = serverSocket.accept();

                System.out.println("Just connected to " + server.getRemoteSocketAddress());
                ObjectInputStream ois = new ObjectInputStream(server.getInputStream());
//                System.out.println(ois.readObject());
                int res = (int) ois.readObject();
                staffSer.setIdCari(res);
                System.out.println(res);

//                DataInputStream in = new DataInputStream(server.getInputStream());
//                System.out.println();
//                System.out.println(in.readUTF());

                ObjectOutputStream oos = new ObjectOutputStream(server.getOutputStream());
                oos.writeObject(staffSer.getIdCari());

//                DataOutputStream out = new DataOutputStream(server.getOutputStream());
//                System.out.println();
//                out.writeUTF("data yang dicari "+staffSer.getIdCari());

            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
//                break;
            }
//        }
    }


    public static void main(String [] args) {
        int port = 6000;
        try {
            Thread t = new ServerConn(port);
            t.start();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
